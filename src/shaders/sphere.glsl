#ifndef SPHERE_GLSL
#define SPHERE_GLSL

#include "ray.glsl"
#include "hit_record.glsl"
#include "sphere_struct.glsl"

HitRecord sphere_hit(Sphere s, Ray r, float t_min, float t_max) {
  vec3 oc = r.origin - s.center;
  float a = dot(r.direction, r.direction);
  float half_b = dot(oc, r.direction);
  float c = dot(oc, oc) - s.radius * s.radius;
  float discriminant = half_b * half_b - a * c;

  HitRecord h_r = hit_init();

  if (discriminant < 0) {
    return h_r;
  }
  float sqrtd = sqrt(discriminant);
  float root = (-half_b - sqrtd) / a;
  if (root < t_min || t_max < root) {
    root = (-half_b + sqrtd) / a;
    if (root < t_min || t_max < root) {
      return h_r;
    }
  }

  h_r.hit = 1;
  h_r.p = ray_at(r, root);
  h_r.normal = (h_r.p - s.center) / s.radius;
  h_r.t = root;
  return h_r;
}

HitRecord sphere_hit_multiple(Ray r, float t_min, float t_max) {
  HitRecord temp_record = hit_init();
  HitRecord ret_record = hit_init();
  float closest = t_max;

  for (int i = 0; i < u_spheres.data.length(); i++) {
   temp_record = sphere_hit(u_spheres.data[i], r, t_min, closest);
   if (temp_record.hit > 0) {
     closest = temp_record.t;
     ret_record = temp_record;
   }
  }

  return ret_record;
}

#endif
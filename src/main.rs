use std::collections::HashMap;
// use std::f32::consts::FRAC_2_PI;
use std::hash::Hash;
use std::sync::Arc;
use std::time::Instant;
use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer, CpuBufferPool, TypedBufferAccess};
use vulkano::command_buffer::{AutoCommandBufferBuilder, CommandBufferUsage, SubpassContents};
use vulkano::descriptor_set::layout::DescriptorSetLayout;
use vulkano::descriptor_set::{PersistentDescriptorSet, WriteDescriptorSet};
use vulkano::device::physical::{PhysicalDevice, PhysicalDeviceType};
use vulkano::device::{Device, DeviceExtensions, Features};
use vulkano::image::view::ImageView;
use vulkano::image::{ImageAccess, ImageUsage, SwapchainImage};
use vulkano::instance::Instance;
use vulkano::pipeline::graphics::input_assembly::InputAssemblyState;
use vulkano::pipeline::graphics::vertex_input::BuffersDefinition;
use vulkano::pipeline::graphics::viewport::{Viewport, ViewportState};
use vulkano::pipeline::{GraphicsPipeline, Pipeline, PipelineBindPoint};
use vulkano::render_pass::{Framebuffer, RenderPass, Subpass};
use vulkano::swapchain::{self, AcquireError, Swapchain, SwapchainCreationError};
use vulkano::sync::{self, FlushError, GpuFuture};
use vulkano::Version;
use vulkano_win::VkSurfaceBuild;
use winit::event::{ElementState, Event, VirtualKeyCode, WindowEvent};
use winit::event_loop::{ControlFlow, EventLoop};
use winit::window::{Window, WindowBuilder};

fn main() {
    let required_extensions = vulkano_win::required_extensions();

    let instance =
        Instance::new(None, Version::V1_1, &required_extensions, None).expect(&failure("vulkan"));

    println!("{}", success("vulkan instance"));

    let event_loop = EventLoop::new();
    let surface = WindowBuilder::new()
        .build_vk_surface(&event_loop, instance.clone())
        .unwrap();

    let device_extensions = DeviceExtensions {
        khr_swapchain: true,
        ..DeviceExtensions::none()
    };

    let (physical_device, queue_family) = PhysicalDevice::enumerate(&instance)
        .filter(|&p| p.supported_extensions().is_superset_of(&device_extensions))
        .filter_map(|p| {
            p.queue_families()
                .find(|&q| q.supports_graphics() && surface.is_supported(q).unwrap_or(false))
                .map(|q| (p, q))
        })
        .min_by_key(|(p, _)| match p.properties().device_type {
            PhysicalDeviceType::DiscreteGpu => 0,
            PhysicalDeviceType::IntegratedGpu => 1,
            PhysicalDeviceType::VirtualGpu => 2,
            PhysicalDeviceType::Cpu => 3,
            PhysicalDeviceType::Other => 4,
        })
        .expect("No suitable device has been found");

    println!(
        "Physical device found: {} ({:?})",
        physical_device.properties().device_name,
        physical_device.properties().device_type,
    );

    let (device, mut queues) = Device::new(
        physical_device,
        &Features::none(),
        &physical_device
            .required_extensions()
            .union(&device_extensions),
        [(queue_family, 0.5)].iter().cloned(),
    )
    .expect(&failure("device"));
    println!("{}", success("device"));

    let queue = queues.next().expect("Unable to retrive queue");

    let (mut swapchain, images) = {
        let caps = surface.capabilities(physical_device).unwrap();
        Swapchain::start(device.clone(), surface.clone())
            .num_images(caps.min_image_count)
            .format(caps.supported_formats[0].0)
            .dimensions(surface.window().inner_size().into())
            .usage(ImageUsage::color_attachment())
            .sharing_mode(&queue)
            .composite_alpha(caps.supported_composite_alpha.iter().next().unwrap())
            .build()
            .expect(&failure("swapchain"))
    };
    println!("{}", success("swapchain"));

    #[repr(C)]
    #[derive(Default, Debug, Clone)]
    struct Vertex {
        position: [f32; 2],
    }
    vulkano::impl_vertex!(Vertex, position);

    let vertex_buffer = CpuAccessibleBuffer::from_iter(
        device.clone(),
        BufferUsage::all(),
        false,
        [
            Vertex {
                position: [1.0, 1.0],
            },
            Vertex {
                position: [1.0, -1.0],
            },
            Vertex {
                position: [-1.0, 1.0],
            },
            Vertex {
                position: [-1.0, -1.0],
            },
        ]
        .iter()
        .cloned(),
    )
    .expect(&failure("vertex buffer"));
    println!("{}", success("vertex bufer"));

    let index_buffer = CpuAccessibleBuffer::from_iter(
        device.clone(),
        BufferUsage::all(),
        false,
        [0u32, 1, 2, 3, 1, 2].iter().cloned(),
    )
    .expect(&failure("index buffer"));
    println!("{}", success("index bufer"));

    mod vs {
        vulkano_shaders::shader! {
            ty: "vertex",
            path: "src/shaders/shader.vert",
        }
    }
    mod fs {
        vulkano_shaders::shader! {
            ty: "fragment",
            path: "src/shaders/shader.frag",
        }
    }

    let vs = vs::load(device.clone()).expect(&failure("vertex shader"));
    println!("{}", success("vertex shader"));
    let fs = fs::load(device.clone()).expect(&failure("fragment shader"));
    println!("{}", success("fragment shader"));

    let time_uniform_buffer =
        CpuBufferPool::<vs::ty::timeBuf>::new(device.clone(), BufferUsage::all());

    let window_uniform_buffer =
        CpuBufferPool::<vs::ty::winBuf>::new(device.clone(), BufferUsage::all());

    let camera_uniform_buffer =
        CpuBufferPool::<fs::ty::cameraBuf>::new(device.clone(), BufferUsage::all());

    let spheres_uniform_buffer = CpuAccessibleBuffer::from_iter(
        device.clone(),
        BufferUsage::all(),
        false,
        [
            fs::ty::Sphere {
                center: [0.0, 0.0, -5.0],
                radius: 0.25,
            },
            fs::ty::Sphere {
                center: [-0.5, 0.0, -1.0],
                radius: 0.15,
            },
            fs::ty::Sphere {
                center: [-0.65, 0.35, -1.0],
                radius: 0.25,
            },
            fs::ty::Sphere {
                center: [0.0, -0.25, -1.0],
                radius: 0.25,
            },
            fs::ty::Sphere {
                center: [0.0, 0.5, -2.0],
                radius: 0.25,
            },
            fs::ty::Sphere {
                center: [0.0, 0.25, -1.0],
                radius: 0.25,
            },
            fs::ty::Sphere {
                center: [0.0, -100.5, -1.0],
                radius: 100.0,
            },
        ]
        .into_iter(),
    )
    .unwrap();

    let render_pass = vulkano::single_pass_renderpass!(
      device.clone(),
      attachments: {
        color: {
          load: Clear,
          store: Store,
          format: swapchain.format(),
          samples: 1,
        }
      },
      pass: {
        color: [color],
        depth_stencil: {}
      }
    )
    .expect(&failure("render pass"));
    println!("{}", success("render pass"));

    let pipeline = GraphicsPipeline::start()
        .vertex_input_state(BuffersDefinition::new().vertex::<Vertex>())
        .vertex_shader(vs.entry_point("main").unwrap(), ())
        .input_assembly_state(InputAssemblyState::new())
        .viewport_state(ViewportState::viewport_dynamic_scissor_irrelevant())
        .fragment_shader(fs.entry_point("main").unwrap(), ())
        .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
        .build(device.clone())
        .expect(&failure("pipeline"));
    println!("{}", success("pipeline"));

    let mut viewport = Viewport {
        origin: [0.0, 0.0],
        dimensions: [0.0, 0.0],
        depth_range: 0.0..1.0,
    };

    let mut framebuffers = window_size_dependent_setup(&images, render_pass.clone(), &mut viewport);

    let mut recreate_swapchain = false;

    let mut previous_frame_end = Some(sync::now(device.clone()).boxed());

    let start_time = Instant::now();
    let mut last_frame = Instant::now();

    let mut key_state: HashMap<VirtualKeyCode, ElementState> = HashMap::new();

    let mut cam = fs::ty::cameraBuf {
        position: [0.0, 0.0, 0.0],
        look_dir: [1.0, 0.0, 0.0],
        _dummy0: [0u8; 4],
    };

    let cam_movement_speed = 1.0;
    let cam_rot_speed = 1.0;

    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent {
            event: WindowEvent::CloseRequested,
            ..
        } => {
            println!("Exiting gracefully...");
            *control_flow = ControlFlow::Exit;
        }
        Event::WindowEvent {
            event: WindowEvent::Resized(_),
            ..
        } => {
            recreate_swapchain = true;
        }
        Event::WindowEvent {
            event:
                WindowEvent::KeyboardInput {
                    input: keyboard_input,
                    ..
                },
            ..
        } => {
            // println!("{:?}", keyboard_input);
            if keyboard_input.virtual_keycode.is_some() {
                key_state.insert(
                    keyboard_input.virtual_keycode.unwrap(),
                    keyboard_input.state,
                );
            }
        }
        Event::RedrawEventsCleared => {
            if get_or(&key_state, &VirtualKeyCode::Escape, &ElementState::Released)
                == &ElementState::Pressed
            {
                *control_flow = ControlFlow::Exit;
            }
            if control_flow == &ControlFlow::Exit {
                return;
            }
            let delta_time = last_frame.elapsed().as_secs_f32();
            let delta_time_millis = last_frame.elapsed().as_millis();
            let fps = if delta_time == 0.0 {
                0.0
            } else {
                1.0 / delta_time
            };
            for _ in 0..2 {
                print!("\u{1B}[A\u{1B}[K");
            }
            println!("{} FPS", fps.round());
            println!("Frame time: {}ms", delta_time_millis);
            last_frame = Instant::now();
            let forward_direction = cam.look_dir;
            let sideways_direction = [-forward_direction[2], 0.0, forward_direction[0]];
            // println!("{:?} {:?}", cam.position, forward_direction);
            if get_or(&key_state, &VirtualKeyCode::W, &ElementState::Released)
                == &ElementState::Pressed
            {
                cam.position[0] += forward_direction[0] * cam_movement_speed * delta_time;
                cam.position[1] += forward_direction[1] * cam_movement_speed * delta_time;
                cam.position[2] += forward_direction[2] * cam_movement_speed * delta_time;
            }
            if get_or(&key_state, &VirtualKeyCode::S, &ElementState::Released)
                == &ElementState::Pressed
            {
                cam.position[0] -= forward_direction[0] * cam_movement_speed * delta_time;
                cam.position[1] -= forward_direction[1] * cam_movement_speed * delta_time;
                cam.position[2] -= forward_direction[2] * cam_movement_speed * delta_time;
            }
            if get_or(&key_state, &VirtualKeyCode::A, &ElementState::Released)
                == &ElementState::Pressed
            {
                cam.position[0] -= sideways_direction[0] * cam_movement_speed * delta_time;
                cam.position[1] -= sideways_direction[1] * cam_movement_speed * delta_time;
                cam.position[2] -= sideways_direction[2] * cam_movement_speed * delta_time;
            }
            if get_or(&key_state, &VirtualKeyCode::D, &ElementState::Released)
                == &ElementState::Pressed
            {
                cam.position[0] += sideways_direction[0] * cam_movement_speed * delta_time;
                cam.position[1] += sideways_direction[1] * cam_movement_speed * delta_time;
                cam.position[2] += sideways_direction[2] * cam_movement_speed * delta_time;
            }
            if get_or(&key_state, &VirtualKeyCode::LShift, &ElementState::Released)
                == &ElementState::Pressed
            {
                cam.position[1] -= cam_movement_speed * delta_time;
            }
            if get_or(&key_state, &VirtualKeyCode::Space, &ElementState::Released)
                == &ElementState::Pressed
            {
                cam.position[1] += cam_movement_speed * delta_time;
            }

            if get_or(&key_state, &VirtualKeyCode::Left, &ElementState::Released)
                == &ElementState::Pressed
            {
                let mut angle = cam.look_dir[0].atan2(cam.look_dir[2]);
                angle += cam_rot_speed * delta_time;
                cam.look_dir[0] = angle.sin();
                cam.look_dir[2] = angle.cos();
                let mag = (cam.look_dir[0] * cam.look_dir[0]
                    + cam.look_dir[1] * cam.look_dir[1]
                    + cam.look_dir[2] * cam.look_dir[2])
                    .sqrt();
                cam.look_dir[0] /= mag;
                // cam.look_dir[1] /= mag;
                cam.look_dir[2] /= mag;
            }
            if get_or(&key_state, &VirtualKeyCode::Right, &ElementState::Released)
                == &ElementState::Pressed
            {
                let mut angle = cam.look_dir[0].atan2(cam.look_dir[2]);
                angle -= cam_rot_speed * delta_time;
                cam.look_dir[0] = angle.sin();
                cam.look_dir[2] = angle.cos();
                let mag = (cam.look_dir[0] * cam.look_dir[0]
                    + cam.look_dir[1] * cam.look_dir[1]
                    + cam.look_dir[2] * cam.look_dir[2])
                    .sqrt();
                cam.look_dir[0] /= mag;
                // cam.look_dir[1] /= mag;
                cam.look_dir[2] /= mag;
            }

            // if key_state
            //     .get(&VirtualKeyCode::Up)
            //     .unwrap_or(&ElementState::Released)
            //     == &ElementState::Pressed
            // {
            //     let mut angle = cam.look_dir[0].atan2(cam.look_dir[1]);
            //     angle -= cam_rot_speed * delta_time;
            //     if angle < -FRAC_2_PI {
            //         angle = -FRAC_2_PI
            //     }
            //     cam.look_dir[0] = angle.sin();
            //     cam.look_dir[1] = angle.cos();
            //     let mag = (cam.look_dir[0] * cam.look_dir[0]
            //         + cam.look_dir[1] * cam.look_dir[1]
            //         + cam.look_dir[2] * cam.look_dir[2])
            //         .sqrt();
            //     cam.look_dir[0] /= mag;
            //     cam.look_dir[1] /= mag;
            //     cam.look_dir[2] /= mag;
            // }
            // if key_state
            //     .get(&VirtualKeyCode::Down)
            //     .unwrap_or(&ElementState::Released)
            //     == &ElementState::Pressed
            // {
            //     let mut angle = cam.look_dir[0].atan2(cam.look_dir[1]);
            //     angle += cam_rot_speed * delta_time;
            //     if angle > FRAC_2_PI {
            //         angle = FRAC_2_PI
            //     }
            //     cam.look_dir[0] = angle.sin();
            //     cam.look_dir[1] = angle.cos();
            //     let mag = (cam.look_dir[0] * cam.look_dir[0]
            //         + cam.look_dir[1] * cam.look_dir[1]
            //         + cam.look_dir[2] * cam.look_dir[2])
            //         .sqrt();
            //     cam.look_dir[0] /= mag;
            //     cam.look_dir[1] /= mag;
            //     cam.look_dir[2] /= mag;
            // }

            previous_frame_end.as_mut().unwrap().cleanup_finished();

            if recreate_swapchain {
                let dimensions: [u32; 2] = surface.window().inner_size().into();
                let (new_swapchain, new_images) =
                    match swapchain.recreate().dimensions(dimensions).build() {
                        Ok(r) => r,
                        Err(SwapchainCreationError::UnsupportedDimensions) => return,
                        Err(e) => panic!("Unable to recreate swapchain: {:?}", e),
                    };

                swapchain = new_swapchain;
                framebuffers =
                    window_size_dependent_setup(&new_images, render_pass.clone(), &mut viewport);
                recreate_swapchain = false;
            }

            let (image_num, suboptimal, acquire_future) =
                match swapchain::acquire_next_image(swapchain.clone(), None) {
                    Ok(r) => r,
                    Err(AcquireError::OutOfDate) => {
                        recreate_swapchain = true;
                        return;
                    }
                    Err(e) => panic!("Unable to acquire image: {:?}", e),
                };
            if suboptimal {
                recreate_swapchain = true;
            }

            let clear_values = vec![[0.1, 0.1, 0.1, 1.0].into()];

            let time_uniform = time_uniform_buffer
                .chunk([vs::ty::timeBuf {
                    elapsed_s: start_time.elapsed().as_secs_f32(),
                }])
                .unwrap();

            let window_uniform = window_uniform_buffer
                .chunk([vs::ty::winBuf {
                    size: surface.window().inner_size().into(),
                }])
                .unwrap();

            let camera_uniform = camera_uniform_buffer.chunk([cam.clone()]).unwrap();

            let descriptor_sets = vec![
                PersistentDescriptorSet::new(
                    get_set_layout(pipeline.clone(), 0),
                    [
                        WriteDescriptorSet::buffer(0, window_uniform.clone()),
                        // WriteDescriptorSet::buffer(1, time_uniform.clone()),
                    ],
                )
                .unwrap(),
                PersistentDescriptorSet::new(
                    get_set_layout(pipeline.clone(), 1),
                    [WriteDescriptorSet::buffer(0, camera_uniform.clone())],
                )
                .unwrap(),
                PersistentDescriptorSet::new(
                    get_set_layout(pipeline.clone(), 2),
                    [WriteDescriptorSet::buffer(
                        0,
                        spheres_uniform_buffer.clone(),
                    )],
                )
                .unwrap(),
            ];

            let mut builder = AutoCommandBufferBuilder::primary(
                device.clone(),
                queue.family(),
                CommandBufferUsage::OneTimeSubmit,
            )
            .expect(&failure("command buffer builder"));

            builder
                .begin_render_pass(
                    framebuffers[image_num].clone(),
                    SubpassContents::Inline,
                    clear_values,
                )
                .unwrap()
                .set_viewport(0, [viewport.clone()])
                .bind_pipeline_graphics(pipeline.clone())
                .bind_descriptor_sets(
                    PipelineBindPoint::Graphics,
                    pipeline.layout().clone(),
                    0,
                    descriptor_sets.clone(),
                )
                .bind_vertex_buffers(0, vertex_buffer.clone())
                .bind_index_buffer(index_buffer.clone())
                .draw_indexed(index_buffer.len() as u32, 1, 0, 0, 0)
                .unwrap()
                .end_render_pass()
                .unwrap();

            let command_buffer = builder.build().expect(&failure("comand buffer"));

            let future = previous_frame_end
                .take()
                .unwrap()
                .join(acquire_future)
                .then_execute(queue.clone(), command_buffer)
                .unwrap()
                .then_swapchain_present(queue.clone(), swapchain.clone(), image_num)
                .then_signal_fence_and_flush();

            match future {
                Ok(future) => {
                    previous_frame_end = Some(future.boxed());
                }
                Err(FlushError::OutOfDate) => {
                    recreate_swapchain = true;
                    previous_frame_end = Some(sync::now(device.clone()).boxed());
                }
                Err(e) => {
                    println!("Failed to flush future: {:?}", e);
                    previous_frame_end = Some(sync::now(device.clone()).boxed());
                }
            }
        }
        _ => (),
    });
}

fn get_set_layout(pipeline: Arc<GraphicsPipeline>, set: usize) -> Arc<DescriptorSetLayout> {
    pipeline
        .layout()
        .descriptor_set_layouts()
        .get(set)
        .unwrap()
        .clone()
}

fn window_size_dependent_setup(
    images: &[Arc<SwapchainImage<Window>>],
    render_pass: Arc<RenderPass>,
    viewport: &mut Viewport,
) -> Vec<Arc<Framebuffer>> {
    let dimentions = images[0].dimensions().width_height();
    viewport.dimensions = [dimentions[0] as f32, dimentions[1] as f32];

    images
        .iter()
        .map(|image| {
            let view = ImageView::new(image.clone()).unwrap();
            Framebuffer::start(render_pass.clone())
                .add(view)
                .unwrap()
                .build()
                .unwrap()
        })
        .collect::<Vec<_>>()
}

fn get_or<'a, Q, V>(map: &'a HashMap<Q, V>, k: &'a Q, d: &'a V) -> &'a V
where
    Q: Hash,
    Q: Eq,
{
    map.get(k).unwrap_or(d)
}

fn success(msg: &str) -> String {
    format!("Successfully instantiated {}", msg)
}

fn failure(msg: &str) -> String {
    format!("Unable to instantiate {}", msg)
}

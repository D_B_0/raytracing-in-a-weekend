#ifndef HIT_RECORD_GLSL
#define HIT_RECORD_GLSL

struct HitRecord {
  int hit;
  vec3 p;
  vec3 normal;
  float t;
};

HitRecord hit_init() {
  HitRecord h_r;
  h_r.hit = 0;
  h_r.p = vec3(0);
  h_r.normal = vec3(0);
  h_r.t = 0;
  return h_r;
}

#endif
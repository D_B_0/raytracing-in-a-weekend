#version 450

layout(set = 0, binding = 1) uniform timeBuf {
  float elapsed_s;
} u_time;

layout(set = 0, binding = 0) uniform winBuf {
  vec2 size;
} u_window;

layout(location = 0) in vec2 position;

void main() {
  gl_Position = vec4(position, 0.0, 1.0);
}
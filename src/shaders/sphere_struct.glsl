#ifndef SPHERE_STRUCT_GLSL
#define SPHERE_STRUCT_GLSL

struct Sphere {
  vec3 center;
  float radius;
};

#endif
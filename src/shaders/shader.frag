#version 450

layout(set = 0, binding = 0) uniform winBuf {
  vec2 size;
} u_window;

layout(set = 0, binding = 1) uniform timeBuf {
  float elapsed_s;
} u_time;

layout(set = 1, binding = 0) uniform cameraBuf {
  vec3 position;
  vec3 look_dir;
} u_cam;

#include "ray.glsl"
#include "hit_record.glsl"

#include "sphere_struct.glsl"
layout(set = 2, binding = 0) readonly buffer spheresBuf {
  Sphere data[];
} u_spheres;
#include "sphere.glsl"

layout(location = 0) out vec4 f_color;

// Gold Noise ©2015 dcerisano@standard3d.com
// - based on the Golden Ratio
// - uniform normalized distribution
// - fastest static noise generator function (also runs at low precision)
// - use with indicated seeding method. 

float PHI = 1.61803398874989484820459;  // Φ = Golden Ratio   

float gold_noise(vec2 xy, float seed) {
  return fract(tan(distance(xy*PHI, xy)*seed)*xy.x);
}

vec3 point_in_sphere(vec2 xy) {
  float i = 0.0;
  while (true) {
    vec3 point = vec3(gold_noise(xy, length(xy) + i + 1.0),
                      gold_noise(xy, length(xy) + i + 2.0),
                      gold_noise(xy, length(xy) + i + 3.0));
    point -= 0.5;
    if (length(point) < 1.0)
      return point;
    i += 1.0;
  }
}

vec3 ray_color(Ray r) {
  HitRecord h_r = sphere_hit_multiple(r, 0, 1000000);
  if (h_r.hit != 0) {
    vec3 target = h_r.p + h_r.normal + point_in_sphere(h_r.p.xz * 100);
    Ray r_diffusion;
    r_diffusion.origin = h_r.p;
    r_diffusion.direction = target - h_r.p;
    return 0.5 * ray_color(r_diffusion);
    // return (h_r.normal + 1) * 0.5;
  }

  float t = (normalize(r.direction).y + 1) * 0.5;
  return mix(vec3(0.5, 0.7, 1.0), vec3(1.0, 1.0, 1.0), t);
}

void main() {
  // The coordinates go from 0 to u_window.size
  // so we resize them to be between -1 and 1.
  // First we send them to the range 0..1
  vec2 uv = gl_FragCoord.xy / u_window.size;
  // then to -1..1
  uv -= vec2(0.5);
  uv *= 2;
  uv.y = -uv.y;
  // The coordinates are now between -1 and 1,
  // but this may cause stretching if the resolution is not 1:1.
  // This may be desirable in some applications,
  // but here we correct for it
  float ratio = max(u_window.size.x, u_window.size.y) / min(u_window.size.x, u_window.size.y);
  float pixel_dim = 2.0 / min(u_window.size.x, u_window.size.y);
  if (u_window.size.x > u_window.size.y) {
    uv.x *= ratio;
  } else {
    uv.y *= ratio;
  }

  // TODO: Expose this to rust and make it configurable
  int antialiasing_subsample = 3;
  vec3 col = vec3(0);
  for (int i = 0; i < antialiasing_subsample; i++) {
    float i_adjust = (float(i + 1) / float(antialiasing_subsample + 2) - 0.5) * pixel_dim;
    for (int j = 0; j < antialiasing_subsample; j++) {
      float j_adjust = (float(j + 1) / float(antialiasing_subsample + 2) - 0.5) * pixel_dim;
      Ray r;
      r.origin = u_cam.position;
      r.direction = normalize(u_cam.look_dir);
      vec3 horizzontal_perp = normalize(vec3(-u_cam.look_dir.z, 0.0, u_cam.look_dir.x));
      vec3 vertical_perp = vec3(0.0, 1.0, 0.0);
      // vec3 vertical_perp = normalize(vec3(1.0,
      //                                     -(
      //                                       (u_cam.look_dir.x + (
      //                                         (u_cam.look_dir.z*u_cam.look_dir.z) / u_cam.look_dir.x
      //                                       ))
      //                                       /
      //                                       u_cam.look_dir.y
      //                                     ),
      //                                     u_cam.look_dir.z / u_cam.look_dir.x));
      r.direction = normalize(r.direction + (uv.x + i_adjust) * horizzontal_perp + (uv.y + j_adjust) * vertical_perp);
      col += ray_color(r);
    }
  }
  col /= antialiasing_subsample * antialiasing_subsample;

  f_color = vec4(col, 1.0);
}
